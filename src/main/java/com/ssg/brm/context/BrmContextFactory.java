package com.ssg.brm.context;

import com.ssg.brm.connpool.ConnectionFactory;

/**
 * Description: Factory for BrmContext instances.
 * 
 * Role: TODO
 * Usage: TODO
 *
 * @author jlacy
 * @company SSG, Ltd.
 *
 * Change-log
 * ------------------------------------------------------------
 * Dec 14, 2011   jlacy    Initial Version
 * ------------------------------------------------------------
 *
 */
public class BrmContextFactory {

    private static BrmContextFactory theInstance;

    public static BrmContextFactory getInstance() {
        if (theInstance == null) {
            theInstance = new BrmContextFactory();
        }
        return theInstance;
    }

    private String programName = "Unknown";
    private ConnectionFactory connectionFactory; // BRM connection pool

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    /**
     * Set this instance as the static singleton instance accessible via BrmContextFactory.getInstance().
     * This is intended to be used where the BrmContextFactory is created as a non-singleton bean (e.g. in Spring)
     * but the static reference still needs to be supported (for example from a ThreadLocal's initialValue() method).
     */
    public void setAsStaticInstance() {
        theInstance = this;
    }

    public BrmContext getContext() {
        if (connectionFactory == null) {
            throw new RuntimeException(
            "BrmContextFactory not properly initialized.  Attempt to get a BrmContext without a ConnectionFactory reference set.");
        }

        BrmContext context = new BrmContext();
        context.setProgramName(programName);
        context.setConnectionFactory(connectionFactory);

        return context;
    }

}
