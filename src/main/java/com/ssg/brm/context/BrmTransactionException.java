package com.ssg.brm.context;

/**
 * Description: TODO
 * Role: TODO
 * Usage: TODO
 *
 * @author jlacy
 * @company SSG, Ltd.
 *
 * Change-log
 * ------------------------------------------------------------
 * Dec 15, 2011   jlacy    Initial Version
 * ------------------------------------------------------------
 *
 */
public class BrmTransactionException extends RuntimeException {

    /**
     * 
     */
    public BrmTransactionException() {
        // TODO Auto-generated constructor stub

    }

    /**
     * @param arg0
     */
    public BrmTransactionException(String arg0) {
        // TODO Auto-generated constructor stub
        super(arg0);
    }

    /**
     * @param arg0
     */
    public BrmTransactionException(Throwable arg0) {
        // TODO Auto-generated constructor stub
        super(arg0);
    }

    /**
     * @param arg0
     * @param arg1
     */
    public BrmTransactionException(String arg0, Throwable arg1) {
        // TODO Auto-generated constructor stub
        super(arg0, arg1);
    }

}
