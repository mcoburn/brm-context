package com.ssg.brm.context;

import com.portal.pcm.PortalContext;

/**
 * Description: Represents the transaction types supporte by BRM.
 * Role: TODO
 * Usage: TODO
 *
 * @author jlacy
 * @company SSG, Ltd.
 *
 * Change-log
 * ------------------------------------------------------------
 * Dec 15, 2011   jlacy    Initial Version
 * ------------------------------------------------------------
 *
 */
public enum BrmTransactionType {

    READONLY(PortalContext.TRAN_OPEN_READONLY),
    READ_WRITE(PortalContext.TRAN_OPEN_READWRITE),
    READ_WRITE_LOCK(PortalContext.TRAN_OPEN_LOCK_OBJ); // TODO NEED TO COMBINE THE INT FLAGS FOR READ_WRITE AND LOCK_OBJ

    private int flag;

    private BrmTransactionType(int pcmFlag) {
        flag = pcmFlag;
    }

    public int getPcmFlag() {
        return flag;
    }

}
