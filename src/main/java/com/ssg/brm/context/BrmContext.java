package com.ssg.brm.context;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import com.portal.pcm.EBufException;
import com.portal.pcm.FList;
import com.portal.pcm.Poid;
import com.portal.pcm.PortalContext;
import com.portal.pcm.PortalOp;
import com.portal.pcm.fields.FldPoid;
import com.portal.pcm.fields.FldVirtualT;
import com.ssg.brm.connpool.Connection;
import com.ssg.brm.connpool.ConnectionFactory;
import com.ssg.brm.exception.PCMException;

/**
 * Description: TODO
 * Role: TODO
 * Usage: TODO
 *
 * @author jlacy
 * @company SSG, Ltd.
 *
 * Change-log
 * ------------------------------------------------------------
 * Dec 4, 2011   jlacy    Initial Version
 * ------------------------------------------------------------
 *
 */
public class BrmContext {

    private static Logger logger = Logger.getLogger(BrmContext.class);

    // ThreadLocal for binding a unique BrmContext instance to each Thread

    private static final ThreadLocal<BrmContext> threadContext =
        new ThreadLocal<BrmContext>() {
        @Override
        protected BrmContext initialValue() {
            // Get a new context from a static factory reference.
            // TODO is there a better way to get ahold of the factory?
            return BrmContextFactory.getInstance().getContext();
        }
    };

    // Static method for obtaining the ThreadLocal resident BrmContext for the current thread.
    // e.g. BrmContext.getInstance().startTransaction();
    public static BrmContext getInstance() {
        return threadContext.get();
    }

    // Resets and removes the current ThreadLocal resident BrmContext.
    // A subsequent attempt to access the ThreadLocal resident BrmContext will result in the creation of a new BrmContext.
    public static void destroyInstance() {
        threadContext.get().reset(); // Cleanup and release a connection if applicable
        threadContext.remove(); // Delete the thread local instance (the next reference will invoke the initialValue() method)
    }


    // inject the factory for obtaining BRM Connections
    private ConnectionFactory connectionFactory;

    // The single BRM Connection managed by this context.
    private Connection connection;

    // Informational attributes.  Passed to BRM for logging/auditing
    private String programName;
    private String userInfo;
    private String sessionId;
    // Generic properties map for hold any application specific data.
    private Map<String, Object> properties = new HashMap<String, Object>();

    /**
     * If a BRM Connection is present,
     * - rollback any transaction.
     * - return the connection back to the pool.
     * Also clears all state including properties, user info, and session id.
     * @throws BrmTransactionExcetpion if unable to rollback transaction
     */
    public void reset() throws BrmTransactionException {
        logger.debug("called reset() on " + this);
        programName = null;
        userInfo = null;
        sessionId = null;
        properties.clear();

        if (connection != null) {
            if (connection.transactionInProgress()) {
                logger.warn(this + " holding Connection with transInProgress=true!!!  Rolling back prior to deallocating.");
                rollbackTransaction(false);
            }
            releaseConnection();
        }
    }

    /**
     * Starts a Read/Write BRM Transaction
     * @throws BrmTransactionException If transaction is already in progress or an error occurs starting a new transaction.
     */
    public void startTransaction() throws BrmTransactionException {
        startTransaction(BrmTransactionType.READ_WRITE);
    }

    public void startTransaction(BrmTransactionType transactionType) throws BrmTransactionException {

        logger.debug("called startTransaction(" + transactionType + ") on " + this);

        connection = getActiveConnection(true);
        if (connection.transactionInProgress()) {
            logger.error("Attempt to start transaction while one is in progress");
            throw new BrmTransactionException("Attempt to start transaction while one is in progress");
        }
        try {
            connection.transactionOpen(transactionType.getPcmFlag());
        } catch (EBufException e) {
            logger.error("Error attempting to start a BRM transaction", e);
            throw new BrmTransactionException("Error attempting to start a BRM transaction", e);
        }
    }

    /**
     * Marks the current transaction as rollback-only.
     * This does not immediately rollback the transaction.  When a commit is attempted, it will perform a rollback instead.
     */
    public void setRollbackOnly() {
        connection = getActiveConnection(true);
        connection.setForceRollback(true);
    }

    public boolean isRollbackOnly() {
        connection = getActiveConnection(true);
        return connection.isForceRollback();
    }

    /**
     * Performs a rollback of the current transaction.
     * @throws BrmTransactionExcetpion if no transaction is currently in progress
     */
    public void rollbackTransaction() throws BrmTransactionException {
        rollbackTransaction(true);
    }

    /**
     * Performs a rollback of the current transaction.
     * @param requireTrans - if true, throws an exception if no transaction is currently in progress.
     * @throws BrmTransactionException if no connection is allocated or no transaction is current in progress
     */
    public void rollbackTransaction(boolean requireTrans) throws BrmTransactionException {

        logger.debug("called rollbackTransaction() on " + this);
        if (connection == null) {
            logger.error("Attempt to rollback transaction with no active connection");
            throw new BrmTransactionException("No connection active.");
        }
        if (!isTransactionInProgress()) {
            if (requireTrans) {
                logger.error("Attempt to rollback transaction when no transaction open (requireTrans=true)");
                throw new BrmTransactionException("No transaction active.");
            }
            else {
                return;
            }
        }

        try {
            connection.transactionAbort();
        } catch (EBufException e) {
            logger.error("Error attempting to rollback BRM transaction.", e);
            throw new BrmTransactionException("Error attempting to rollback BRM transaction.", e);
        }
        releaseConnection();
    }

    // Really need to return a boolean indicating success (to be able to tell if the commit commited or
    // if forceRollback was set and it rolled back.
    // The problem is that the PortalContext.commitTransaction signature being overriden returns void.
    public void commitTransaction() throws BrmTransactionException {
        logger.debug("called commitTransaction() on " + this);
        if (connection == null) {
            logger.error("Attempt to commit transaction with no active connection");
            throw new BrmTransactionException("No connection active.");
        }
        if (!isTransactionInProgress()) {
            logger.error("Attempt to commit a transaction when none is open");
            throw new BrmTransactionException("No transaction active.");
        }

        try {
            connection.transactionCommit();
        } catch (EBufException e) {
            logger.error("Error attempting to commit BRM transaction.", e);
            throw new BrmTransactionException("Error attempting to commit BRM transaction.", e);
        }
        releaseConnection(); // ?? Is this necessary?
    }

    public boolean isTransactionInProgress() {
        Connection c = getActiveConnection(false);
        if (c == null) {
            return false;
        }
        return c.transactionInProgress();
    }


    public Connection getActiveConnection() {
        return getActiveConnection(false);
    }

    public Connection getActiveConnection(boolean allocateIfNull) {

        if (connection == null && allocateIfNull) {
            try {
                connection = connectionFactory.getConnection(this, null);
            } catch (PCMException e) {
                logger.error("Unable to allocate a BRM Connection", e);
                throw new RuntimeException("Unable to allocate a BRM Connection", e);
            }
            logger.debug("Allocated new connection for " + this);

            // Set the current user information into the connection for use if auditing is enabled.
            // (See PooledConnectionFactory.setAuditEnabled())
            if (userInfo != null) {
                connection.setUser(userInfo);
            }
        }

        return connection;
    }


    public void releaseConnection() {
        if (connection == null) {
            logger.warn("Attempt to release null Connection!");
            return;
        }
        logger.debug("called releaseConnection() on " + this);

        try {
            connection.setUser(null);
            connection.close(false); // Actually returns connection to pool. Note: boolean arg is ignored by PooledConnectionFactory.PooledConnection
        } catch (EBufException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        connection = null;
    }

    public String getBrmCorrelationId() {
        Connection conn = getActiveConnection(false);
        if (conn != null && conn.getCorrelationId() == null) {
            conn.setCorrelationId(PortalContext.genCorrelationId());
        }
        return (conn != null) ? conn.genCorrelationId() : "";
    }


    // Should this be protected?
    public ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public String getProgramName() {
        return programName;
    }
    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getUserInfo() {
        return userInfo;
    }
    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo;
    }

    public String getSessionId() {
        return sessionId;
    }
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public long getDatabaseNumber() {
        // Is there a better way to get this?
        return connectionFactory.getDatabaseNumber();
    }

    /**
     * Returns the current time within BRM (pin virtual time).
     * This may vary from system time in test environments where pin virtual time has been moved forward to facilitate
     * billing testing.
     * @return
     */
    public Date getVirtualTime() {
        Date result = null;
        Connection c = getActiveConnection(true);
        if (c != null) {
            try {
                FList inputFList = new FList();
                inputFList.set(FldPoid.getInst(), new Poid(getDatabaseNumber(), -1, "/"));
                FList outputFList = c.opcode(PortalOp.GET_PIN_VIRTUAL_TIME, inputFList);
                result = outputFList.get(FldVirtualT.getInst());
            } catch (EBufException e) {
                throw new RuntimeException("Error obtaining virtual time result from output FList", e);
            }
        }
        return result;
    }

    public void setProperty(String name, Object value) {
        properties.put(name, value);
    }

    public Object getProperty(String name) {
        return properties.get(name);
    }

    @Override
    public String toString() {
        return "BrmContext[" + this.hashCode() + ":" + (connection != null ? connection.toString() : "null") + "]";
    }

}
