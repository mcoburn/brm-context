package com.ssg.brm.context;

/**
 * Description: Defines the 3 transaction propagation types that BRM supports.
 * 
 * Role: TODO
 * Usage: TODO
 *
 * @author jlacy
 * @company SSG, Ltd.
 *
 * Change-log
 * ------------------------------------------------------------
 * Dec 15, 2011   jlacy    Initial Version
 * ------------------------------------------------------------
 *
 */
public enum BrmTransactionPropagationType {

    /*
    This opcode manages transactions internally to ensure absolute integrity of the database.
    A transaction for this opcode cannot be wrapped in another transaction.
    If no transaction is open when the opcode is called, a read-write transaction is automatically opened
    and all actions are performed within this transaction.
    If a transaction is already open when the opcode is called, an error occurs.
     */
    REQUIRES_NEW,

    /*
    The transaction for this opcode can be wrapped in a transaction opened by another opcode.
    If a read-write transaction is already open when this opcode is executed, all data modifications take place within the open transaction.
        The modifications are committed or aborted along with all other changes when the transaction is committed or aborted.
    If no transaction is open when the opcode is called, a read-write transaction is opened. All actions are performed within this
    transaction, ensuring that the entire operation is performed atomically. If an error occurs during the execution of the opcode,
    all changes are aborted when the transaction is aborted. If no error occurs, the transaction is committed at the end of the operation.
    This opcode requires a read-write transaction. It is therefore an error to have a read-only transaction open when this opcode is called.
     */
    REQUIRED,

    /*
    This opcode does not modify object data. If it is called while a transaction is not already open, the operation is executed without transactional control.
    If a read-write or read-only transaction is already open when this opcode is called, the opcode is executed as part of the transaction and reads the in-process state of the data.
    If the opcode is called when a separate, unrelated transaction is taking place, it reads the last saved state of the database.
     */
    SUPPORTS;

}
